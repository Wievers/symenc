# SymEnc AES 256 encryption/decryption using pycrypto library
import sys
import subprocess
import base64
import hashlib

try:
    from Crypto.Cipher import AES
    from Crypto import Random

except ModuleNotFoundError as e:
    print(e)


BLOCK_SIZE = 16
unpad = lambda s: s[:-ord(s[len(s) - 1:])] #le pendant du padding mais dans l'autre sens
#Faire 2 lambdas ou 2 méthodes, flemme, TODO

class fichierCrypt:
    """Class crypting/decrypting a file using SHA256 and AES (very reliable technique), using a password
        - mdp [str] : Password used to create a key
        - fichierBase : File to crypt/decrypt"""
    def __init__(self, mdp, fichierBase):
        self.fichier = fichierBase
        self.password = mdp
        self.block_size = 16

    def _pad(self, s):
        """Method padding the data to match the data length of cryptography algorithm used"""
        # pad with bytes instead of str
        return s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * \
        chr(BLOCK_SIZE - len(s) % BLOCK_SIZE).encode('utf8')

    def encrypt(self, create = False):
        """Method crypting a file"""
        file_enc = open(self.fichier, "rb")
        raw = file_enc.read()
        file_enc.close()
        private_key = hashlib.sha256(self.password.encode("utf-8")).digest()
        #raw = raw.encode("utf-8")
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        if not create: save_file = open(self.fichier, "wb")
        else:
            extWhere = self.fichier.find(".")
            ext = self.fichier[extWhere:]
            name = self.fichier[:extWhere]
            save_file = open("{}_crypt{}".format(name, ext), "wb")

        save_file.write(base64.b64encode(iv + cipher.encrypt(raw)))
        save_file.close()

    def decrypt(self):
        """Method decrypting a file"""
        save_file = open(self.fichier, "rb")
        enc = save_file.read()
        save_file.close()
        private_key = hashlib.sha256(self.password.encode("utf-8")).digest()
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        try:
            extWhere = self.fichier.find(".")
            ext = self.fichier[extWhere:]
            name = self.fichier[:extWhere]
            save_file = open("{}_decrypt{}".format(name, ext), "wb")
            save_file.write(unpad(cipher.decrypt(enc[16:])))
            save_file.close()

        except Exception as e:
            print("Error: {}".format(str(e)))
