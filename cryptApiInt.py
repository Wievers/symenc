#SymEnc v1.1 GUI

import sys, re
import json as js
import ntpath as ntp
import subprocess


try:
    from PySide2 import QtWidgets as qtw
    from PySide2.QtCore import Qt, Slot, QAbstractItemModel
    from PySide2 import QtGui as qtg
    from Crypto.Cipher import AES
    from Crypto import Random

except ModuleNotFoundError as e:
    print(e)
    modules = ["PySide2", "PyCryptodome"]
    for i in modules:
        try:
            subprocess.check_call(['pip3', 'install', i])
            # process output with an API in the subprocess module:
            reqs = subprocess.check_output(['pip3', 'freeze'])

        except FileNotFoundError as e:
            print(str(e))
            print("Your system isn't up to date!...")
            print("\033[31m Check the python version, pip3 version and all components required to properly run python programs \033[0m")
            print("\033[31m The program will now stop due to a critical failure \033[0m")
            sys.exit()
            
    print("All packages for the GUI are up-to-date and installed")
    print("\033[31m Restart the program \033[0m ")
    sys.exit()

import cryptTest as cp

class parameters(qtw.QDialog):
    def __init__(self, checked = False, parent = None):
        super(parameters, self).__init__(parent)
        layout = qtw.QVBoxLayout()
        self.newDoc = qtw.QCheckBox("Sauvegarde des fichiers cryptées")
        self.newDoc.setToolTip("Créer un nouveau document crypté, en gardant l'original en clair")
        self.newDoc.setChecked(checked)
        layout.addWidget(self.newDoc)

        self.validParam = qtw.QPushButton("Sauvegarder")
        self.validParam.clicked.connect(self.validate)
        layout.addWidget(self.validParam)

        self.setLayout(layout)

    def validate(self):
        self.close()

class mainWindow(qtw.QMainWindow):
    def __init__(self):
        super(mainWindow, self).__init__()
        layout = qtw.QVBoxLayout()
        self.groupWid = qtw.QGroupBox()
        self.groupWid.setTitle("SymEnc")
        self.setFixedSize(0,0)
        self.setWindowTitle("SymEnc v1.2 GUI")
        self.create = False

        #Création d'un menu
        self.mainMenu = self.menuBar()
        self.menu = [None]
        self.menu[0] = self.mainMenu.addMenu("Outils")

        connect_action = [None]
        connect_action[0] = qtw.QAction("Parameters", self)
        #connect_action[0].setIcon(qtg.QIcon("graphics/export.svg"))
        connect_action[0].triggered.connect(self.param)
        self.menu[0].addAction(connect_action[0])

        self.fileButton = qtw.QPushButton("Select a file")
        self.fileButton.clicked.connect(self.findFile)
        layout.addWidget(self.fileButton)

        self.password = qtw.QLineEdit()
        self.password.setEchoMode(self.password.Password)
        layout.addWidget(self.password)

        layoutCrypt = qtw.QHBoxLayout()
        self.decrypt = qtw.QRadioButton("Décrypter")
        self.crypt = qtw.QRadioButton("Crypter")
        self.crypt.setToolTip("Attention, vous pouvez crypter plusieurs fois un même document, auquel cas il faut utiliser chaque mot de passe dans l'ordre de cryptage")
        self.crypt.setChecked(True)
        layoutCrypt.addWidget(self.crypt)
        layoutCrypt.addWidget(self.decrypt)
        self.complete = qtw.QPushButton("Valider")
        self.complete.setEnabled(False)
        self.complete.clicked.connect(self.valid)
        layoutCrypt.addWidget(self.complete)

        layout.addLayout(layoutCrypt)

        self.groupWid.setLayout(layout)
        self.setCentralWidget(self.groupWid)

    @Slot()
    def param(self):
        buffParam = parameters(self.create, parent = self)
        buffParam.exec()
        self.create = buffParam.newDoc.isChecked()

    @Slot()
    def findFile(self):
        self.file = qtw.QFileDialog()
        self.file.setFileMode(qtw.QFileDialog.ExistingFile)
        self.file.exec_()

        if len(self.file.selectedFiles()) != 0:
            self.fileButton.setText(ntp.basename(self.file.selectedFiles()[0]))
            self.complete.setEnabled(True)

    @Slot()
    def valid(self):
        self.cont = cp.fichierCrypt(self.password.text(), self.file.selectedFiles()[0])
        if self.crypt.isChecked():
            try:
                self.cont.encrypt(self.create)
                val = qtw.QMessageBox(qtw.QMessageBox.Information, "Fenêtre de statut", "Cryptage réussi!")
                val.exec()
            except Exception as e:
                val = qtw.QMessageBox(qtw.QMessageBox.Critical, "Erreur de cryptage", str(e))
                val.exec()
        else:
            try:
                self.cont.decrypt()
                val = qtw.QMessageBox(qtw.QMessageBox.Information, "Fenêtre de statut", "Décryptage réussi!")
                val.exec()
            except Exception as e:
                val = qtw.QMessageBox(qtw.QMessageBox.Critical, "Erreur de décryptage", str(e))
                val.exec()

if __name__ == '__main__':
    # Create the Qt Application
    app = qtw.QApplication(sys.argv)
    # Create and show the form
    form = mainWindow()
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
